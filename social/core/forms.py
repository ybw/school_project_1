"""Form module
"""
from django import forms
from django.contrib.auth.models import User
from social.userauth.models import Profile


class ProfileForm(forms.ModelForm):
    """This form provide fields to `profile settings` page
    """
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),
                                 max_length=30,
                                 required=False)
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),
                                max_length=30,
                                required=False)
    job_title = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),
                                max_length=50,
                                required=False)
    email = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),
                            max_length=75,
                            required=False)
    url = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),
                          max_length=50,
                          required=False)
    location = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),
                               max_length=50,
                               required=False)
    address = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),
                               max_length=50,
                               required=False)
    telephone = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),
                               max_length=50,
                               required=False)
    workplace = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}),
                               max_length=50,
                               required=False)

    class Meta:
        """This class used to override automatic generated fields
        """
        model = User
        fields = ['first_name', 'last_name', 'job_title', 'email', 'url', 'location', 'address', 'telephone', 'workplace']


class PictureForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('picture',)


class ChangePasswordForm(forms.ModelForm):
    """This form provide fields for ``change password`` page
    """
    id = forms.CharField(widget=forms.HiddenInput())
    old_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                                   label="Old password",
                                   required=True)

    new_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                                   label="New password",
                                   required=True)
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}),
                                       label="Confirm new password",
                                       required=True)

    class Meta:
        """This class override automatic generated fields.
        """
        model = User
        fields = ['id', 'old_password', 'new_password', 'confirm_password']

    def clean(self):
        """This function validate and reset form for password change

        Return cleaned form
        """
        super(ChangePasswordForm, self).clean()
        old_password = self.cleaned_data.get('old_password')
        new_password = self.cleaned_data.get('new_password')
        confirm_password = self.cleaned_data.get('confirm_password')
        id = self.cleaned_data.get('id')
        user = User.objects.get(pk=id)
        if not user.check_password(old_password):
            self._errors['old_password'] = self.error_class(['Old password don\'t match'])
            if new_password and new_password != confirm_password:
                self._errors['new_password'] = self.error_class(['Passwords don\'t match'])
                return self.cleaned_data
