"""Views module for Core app
"""
import os

import image
from django.conf import settings as django_settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, redirect, get_object_or_404

from social.core.forms import ProfileForm, ChangePasswordForm, PictureForm
from social.feeds.models import Feed
from social.feeds.views import FEEDS_NUM_PAGES
from social.feeds.views import feeds
from django.utils.timezone import now

def home(request):
    """Home page view.
        Check if user authenticated and return HttpResponse.
        Request is HttpRequest object to pass state through the system.

        ``request`` and ``response`` objects to pass state through the system.

    Args:
        request (HttpRequest): Object that contains metadata about the request

    Returns:
        HttpResponse: Object whose content is filled with the result of
        combine a given template with a given context dictionary.
    """
    if request.user.is_authenticated():
        return feeds(request)
    else:
        return render(request, 'core/cover.html')


@login_required
def network(request):
    """View that render page whith all members
    """
    users_list = User.objects.filter(is_active=True).order_by('username')
    paginator = Paginator(users_list, 100)
    page = request.GET.get('page')
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
    return render(request, 'core/network.html', {'users': users})


@login_required
def friends(request):
    """View for friends page
    """
    current_user = request.user
    follows_list = current_user.profile.follows.order_by('user')
    paginator = Paginator(follows_list, 100)
    page = request.GET.get('page')
    try:
        friends = paginator.page(page)
    except PageNotAnInteger:
        friends = paginator.page(1)
    except EmptyPage:
        friends = paginator.page(paginator.num_pages)
    return render(request, 'core/friends.html', {'friends': friends})


@login_required
def recommended_friends(request):
    """View for recommended friends page
    """
    current_user = request.user
    friends_friends = []
    follows_list = current_user.profile.follows.order_by('user')
    for friend in follows_list:
        friends_friends += friend.follows.order_by('user')
    paginator = Paginator(friends_friends, 100)
    page = int(request.GET.get('page', 1))
    try:
        recommended = paginator.page(page)
    except PageNotAnInteger:
        recommended = paginator.page(1)
    except EmptyPage:
        recommended = paginator.page(paginator.num_pages)
    return render(request, 'core/friend_friends.html',
                  {'recommended': recommended, 'current_user': current_user})


@login_required
def profile(request, username):
    """
    View for profile page.
    When new user added to friends or removed - update Recommendation model.
    """
    current_user = request.user
    current_user_id = current_user.id
    page_user = get_object_or_404(User, username=username)
    all_feeds = Feed.get_feeds().filter(user=page_user)
    paginator = Paginator(all_feeds, FEEDS_NUM_PAGES)
    feeds = paginator.page(1)
    from_feed = -1
    if feeds:
        from_feed = feeds[0].id
    follows = current_user.profile.follows.all()
    if 'add' in request.GET:
        page_user.profile.followers.add(current_user_id)
        page_user.profile.save()
    if 'remove' in request.GET:
        page_user.profile.followers.remove(current_user_id)
        page_user.profile.save()
    return render(request, 'core/profile.html', {
        'page_user': page_user,
        'feeds': feeds,
        'from_feed': from_feed,
        'page': 1,
        'current_user': current_user,
        'follows': follows,})


@login_required
def settings(request):
    """View for profile settings page
    If Request method POST and form valid then save settings to database
    If Request method GET then show form
    """
    user = request.user
    if request.method == 'POST':
        form = ProfileForm(request.POST)
        if form.is_valid():
            user.first_name = form.cleaned_data.get('first_name')
            user.last_name = form.cleaned_data.get('last_name')
            user.profile.job_title = form.cleaned_data.get('job_title')
            user.email = form.cleaned_data.get('email')
            user.profile.url = form.cleaned_data.get('url')
            user.profile.location = form.cleaned_data.get('location')
            user.profile.address = form.cleaned_data.get('address')
            user.profile.telephone = form.cleaned_data.get('telephone')
            user.profile.workplace = form.cleaned_data.get('workplace')
            user.profile.save()
            user.save()
            messages.add_message(request, messages.SUCCESS,
                                 'Your profile were successfully edited.')
    else:
        form = ProfileForm(instance=user, initial={
            'job_title': user.profile.job_title,
            'url': user.profile.url,
            'location': user.profile.location,
            'address': user.profile.address,
            'telephone': user.profile.telephone,
            'workplace': user.profile.workplace
        })
    return render(request, 'core/settings.html', {'form': form})


@login_required
def picture(request):
    """View for user_image settings page.
    """
    return render(request, 'core/picture.html',
                  {'profile': request.user.profile})


@login_required
def password(request):
    """View for Password settings page
    """
    user = request.user
    if request.method == 'POST':
        form = ChangePasswordForm(request.POST)
        if form.is_valid():
            new_password = form.cleaned_data.get('new_password')
            user.set_password(new_password)
            user.save()
            messages.add_message(request, messages.SUCCESS,
                                 'Your password were successfully changed.')
    else:
        form = ChangePasswordForm(instance=user)
    return render(request, 'core/password.html', {'form': form})


@login_required
def upload_picture(request):
    """Summary
    """
    if request.method == 'POST':
        form = PictureForm(request.POST, request.FILES, instance=request.user.profile)
        if form.is_valid():
            form.save()
            request.user.profile.picture_update_dt = now()
            request.user.profile.save()
    # try:
    #     profile_pictures = django_settings.MEDIA_ROOT + '/profile_pictures/'
    #     if not os.path.exists(profile_pictures):
    #         os.makedirs(profile_pictures)
    #     f = request.FILES['picture']
    #     filename = profile_pictures + request.user.username + '_tmp.jpg'
    #     with open(filename, 'wb+') as destination:
    #         for chunk in f.chunks():
    #             destination.write(chunk)
    #     im = Image.open(filename)
    #     width, height = im.size
    #     if width > 350:
    #         new_width = 350
    #         new_height = (height * 350) / width
    #         new_size = new_width, new_height
    #         im.thumbnail(new_size, Image.ANTIALIAS)
    #         im.save(filename)
        return redirect('/settings/picture/?upload_picture=uploaded')
    # except Exception as e:
    return redirect('/settings/picture/')


@login_required
def save_uploaded_picture(request):
    """Save pic
    """
    try:
        x = int(request.POST.get('x'))
        y = int(request.POST.get('y'))
        w = int(request.POST.get('w'))
        h = int(request.POST.get('h'))
        tmp_filename = django_settings.MEDIA_ROOT + '/profile_pictures/' + request.user.username + '_tmp.jpg'
        filename = django_settings.MEDIA_ROOT + '/profile_pictures/' + request.user.username + '.jpg'

        im = Image.open(tmp_filename)
        cropped_im = im.crop((x, y, w + x, h + y))
        cropped_im.thumbnail((200, 200), Image.ANTIALIAS)
        cropped_im.save(filename)
        os.remove(tmp_filename)
    except Exception as e:
        pass
    return redirect('/settings/picture/')

# update recommendation
