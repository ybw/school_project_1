"""Activity views for project.
"""
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render

from social.activities.models import Notification
from social.decorators import ajax_required


@login_required
def notifications(request):
    """Mark all unread notifications as read and render the page.

    This view get all unread notifications which belongs to the user
    and marks them as read.

    Args:
        request (HttpRequest): Object that used to pass state through
        the system.

        Used for representing the currently logged-in user. When a page
        is requested, creates an HttpRequest object that
        contains metadata about the request. Then loads the appropriate
        view, passing the HttpRequest as the first argument to the view
        function.

    Returns:
        HttpResponse: Object whose content is filled with the result of
        combine a given template with a given context dictionary.
    """
    user = request.user
    notifications = Notification.objects.filter(to_user=user)
    unread = Notification.objects.filter(to_user=user, is_read=False)
    for notification in unread:
        notification.is_read = True
        notification.save()
    return render(request, 'activities/notifications.html',
                  {'notifications': notifications})


@login_required
@ajax_required
def last_notifications(request):
    """Mark 5 last of the unread notifications as read and render the page.

    This view get last new notifications which belongs to the user and marks
    them as read.

    Args:
        request (HttpRequest): Object that used to pass state through
        the system.

    Returns:
        HttpResponse: Object whose content is filled with the result of
        combine a given template with a given context dictionary.
    """
    user = request.user
    notifications = Notification.objects.filter(
        to_user=user,
        is_read=False)[:5]
    for notification in notifications:
        notification.is_read = True
        notification.save()
    return render(request, 'activities/last_notifications.html', {'notifications': notifications})


@login_required
@ajax_required
def check_notifications(request):
    """Check for new notifications.

    This method every 30 seconds try to get last 5 unread notification
    Used in ``last_notifications`` view.

    Args:
        request (HttpRequest): Object that used to pass state through
        the system.

    Returns:
        HttpResponse: Object contains up to 5 last notifications.
        len() used in frond-end to check if response is null or not.
    """
    user = request.user
    notifications = Notification.objects.filter(
        to_user=user, is_read=False)[:5]
    return HttpResponse(len(notifications))
