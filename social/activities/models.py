"""Model module for Activities package.
"""
from django.contrib.auth.models import User
from django.db import models
from django.utils.html import escape


class Activity(models.Model):
    """This table used to store user activity.

    Attributes:
        activity_type (str): a human-readable name of item in ACTIVITY_TYPES

        ACTIVITY_TYPES (Field.choices): An iterable (e.g., a list or
            tuple) consisting itself of iterable of exactly two items
            (e.g. [(A, B), (A, B) ...]) to use as choices for this field.
            The first element in each tuple is the actual value to be set
            on the model, and the second element is the human-readable name.

        date: DateTimeField. After creation cannot be changed.

        feed (int): Id. Relation throw User.

        LIKE (str):Suitably-named constant for a choice value.

        user: A many-to-one relationship to User model.

    """

    LIKE = 'L'
    ACTIVITY_TYPES = (
        (LIKE, 'Like'),)

    user = models.ForeignKey(User)
    activity_type = models.CharField(max_length=1, choices=ACTIVITY_TYPES)
    date = models.DateTimeField(auto_now_add=True)
    feed = models.IntegerField(null=True, blank=True)

    class Meta:
        """Class used to override automatic generated database table name.

        Attributes:
            verbose_name (str): A human-readable name for the object.
            verbose_name_plural (str): Plural name of the object.
        """
        verbose_name = 'Activity'
        verbose_name_plural = 'Activities'

    def __str__(self):
        """Return representation of the model. (activity_type)"""
        return self.activity_type


class Notification(models.Model):
    """This table used to store user notifications.

    Attributes:
        ALSO_COMMENTED (str): Description
        COMMENTED (str): Description
        date (TYPE): Description
        feed (TYPE): Description
        from_user (TYPE): Description
        is_read (Bool): Description
        LIKED (str): Description
        notification_type (str): A nice name of item in NOTIFICATION_TYPES.
        NOTIFICATION_TYPES (TYPE): Description
        to_user (TYPE): Description
    """
    LIKED = 'L'
    COMMENTED = 'C'
    ALSO_COMMENTED = 'S'
    NOTIFICATION_TYPES = (
        (LIKED, 'Liked'),
        (COMMENTED, 'Commented'),
        (ALSO_COMMENTED, 'Also Commented'),
    )

    # templates
    _LIKED_TEMPLATE = '<a href="/{0}/">{1}</a> liked your post: <a href="/feeds/{2}/">{3}</a>'
    _COMMENTED_TEMPLATE = '<a href="/{0}/">{1}</a> commented on your post: <a href="/feeds/{2}/">{3}</a>'
    _ALSO_COMMENTED_TEMPLATE = '<a href="/{0}/">{1}</a> also commentend on the post: <a href="/feeds/{2}/">{3}</a>'

    # fields of a model
    from_user = models.ForeignKey(User, related_name='+')
    to_user = models.ForeignKey(User, related_name='+')
    date = models.DateTimeField(auto_now_add=True)
    feed = models.ForeignKey('feeds.Feed', null=True, blank=True)
    notification_type = models.CharField(
        max_length=1, choices=NOTIFICATION_TYPES)
    is_read = models.BooleanField(default=False)

    class Meta:
        """Summary

        Attributes:
            ordering (tuple): Description
            verbose_name (str): Description
            verbose_name_plural (str): Description
        """
        verbose_name = 'Notification'
        verbose_name_plural = 'Notifications'
        ordering = ('-date',)

    def __str__(self):
        """Summary

        Returns:
            TYPE: Description
        """
        if self.notification_type == self.LIKED:
            return self._LIKED_TEMPLATE.format(
                escape(self.from_user.username),
                escape(self.from_user.profile.get_screen_name()),
                self.feed.pk,
                escape(self.get_summary(self.feed.post))
            )
        elif self.notification_type == self.COMMENTED:
            return self._COMMENTED_TEMPLATE.format(
                escape(self.from_user.username),
                escape(self.from_user.profile.get_screen_name()),
                self.feed.pk,
                escape(self.get_summary(self.feed.post))
            )
        elif self.notification_type == self.ALSO_COMMENTED:
            return self._ALSO_COMMENTED_TEMPLATE.format(
                escape(self.from_user.username),
                escape(self.from_user.profile.get_screen_name()),
                self.feed.pk,
                escape(self.get_summary(self.feed.post))
            )
        else:
            return 'Ooops! Something went wrong.'

    @staticmethod
    def get_summary(value):
        """Summary

        Args:
            value (TYPE): Description

        Returns:
            TYPE: Description
        """
        summary_size = 50
        if len(value) > summary_size:
            return '{0}...'.format(value[:summary_size])
        else:
            return value
