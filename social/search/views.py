"""
Search views for project.

Included:
    1. **Search** - view for search results or simple search template render
"""
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Q
from django.shortcuts import render, redirect

from social.feeds.models import Feed


@login_required
def search(request):
    """Search function
    return: Search results page or basic search template render
    """
    if 'q' in request.GET:
        # get query string from request
        querystring = request.GET.get('q').strip()
        # check for whitespaces
        if len(querystring) == 0:
            return redirect('/search/')
        try:
            # try to get search type
            search_type = request.GET.get('type')
            # if can't get search type set it to `feed`
            if search_type not in ['feed', 'users']:
                search_type = 'feed'
        except Exception as e:
            search_type = 'feed'

        count = {}
        # search contains in `user` and `feed`
        results = {'feed': Feed.objects.filter(post__icontains=querystring, parent=None), 'users': User.objects.filter(
            Q(username__icontains=querystring) | Q(first_name__icontains=querystring) | Q(
                last_name__icontains=querystring))}
        # set counts of finds objects
        count['feed'] = results['feed'].count()
        count['users'] = results['users'].count()

        # redner template with resulst
        return render(request, 'search/results.html', {
            'hide_search': True,
            'querystring': querystring,
            'active': search_type,
            'count': count,
            'results': results[search_type],
        })
    else:
        # render simple template with search field
        return render(request, 'search/search.html', {'hide_search': True})
