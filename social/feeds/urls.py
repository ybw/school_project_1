"""
Feeds urls for project.

Included:
    1. **/** - timeline
    2. **/post/** - create new feed
    3. **/like/** - like feed
    4. **/comment/** - comment feed
    5. **/load/** - load feed
    6. **/check/** - check for new feeds,likes,comments
    7. **/load_new/** - load new feeds
    8. **/update/** - update count of likes and comments
    9. **/track_comments/** - load comments
    10. **/remove/** - remove feed
    11. **/feed_url/** - direct link to feed page

Attributes:
    urlpatterns (TYPE): Description
"""
from django.conf.urls import patterns, url

urlpatterns = patterns('social.feeds.views',
                       url(r'^$', 'feeds', name='feeds'),
                       url(r'^post/$', 'post', name='post'),
                       url(r'^like/$', 'like', name='like'),
                       url(r'^comment/$', 'comment', name='comment'),
                       url(r'^load/$', 'load', name='load'),
                       url(r'^check/$', 'check', name='check'),
                       url(r'^load_new/$', 'load_new', name='load_new'),
                       url(r'^update/$', 'update', name='update'),
                       url(r'^track_comments/$', 'track_comments', name='track_comments'),
                       url(r'^remove/$', 'remove', name='remove_feed'),
                       url(r'^(\d+)/$', 'feed', name='feed'),
                       )
