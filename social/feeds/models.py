"""Summary
"""
import bleach
from django.contrib.auth.models import User
from django.db import models
from django.utils.html import escape
from django.utils.translation import ugettext_lazy as _

from social.activities.models import Activity


class Feed(models.Model):
    """
    This class keeps model for `feed`. Feed it is a short post by user on his page.

    Attributes:
        comments (TYPE): Description
        date (TYPE): Description
        likes (TYPE): Description
        parent (TYPE): Description
        post (TYPE): Description
        user (TYPE): Description
    """
    user = models.ForeignKey(User)
    date = models.DateTimeField(auto_now_add=True)
    post = models.TextField(max_length=255)
    parent = models.ForeignKey('Feed', null=True, blank=True)
    likes = models.IntegerField(default=0)
    comments = models.IntegerField(default=0)

    class Meta:
        """
        This class used to override automatic generated database table name.

        Attributes:
            ordering (tuple): Description
            verbose_name (TYPE): Description
            verbose_name_plural (TYPE): Description
        """
        verbose_name = _('Feed')
        verbose_name_plural = _('Feeds')
        ordering = ('-date',)

    def __unicode__(self):
        """
        Used to display human-readable representation of the model
        """
        return self.post

    @staticmethod
    def get_feeds(from_feed=None):
        """
        :param from_feed: id of last loaded feed
        :type from_feed: int
        :return: All feeds which do not have parent(not child) feed. If `from_feed` in request - load feeds from this id.
        :rtype: QuerySet

        Args:
            from_feed (TYPE, optional): Description
        """
        if from_feed is not None:
            feeds = Feed.objects.filter(parent=None, id__lte=from_feed)
        else:
            feeds = Feed.objects.filter(parent=None)
        return feeds

    @staticmethod
    def get_feeds_after(feed):
        """
        :param feed: Feed object
        :type feed: object
        :return: QuerySet containing feeds that have id higher than `feed`
        :rtype: QuerySet

        Args:
            feed (TYPE): Description
        """
        feeds = Feed.objects.filter(parent=None, id__gt=feed)
        return feeds

    def get_comments(self):
        """
        :return: QuerySet containing comments for `feed`
        :rtype: QuerySet
        """
        return Feed.objects.filter(parent=self).order_by('date')

    def calculate_likes(self):
        """
        This function calculate likes of `feed`
        :return: Number of likes for `feed`
        :rtype: int
        """
        likes = Activity.objects.filter(activity_type=Activity.LIKE, feed=self.pk).count()
        self.likes = likes
        self.save()
        return self.likes

    def get_likes(self):
        """
        :return: QuerySet containing likes for `feed`
        :rtype: QuerySet
        """
        likes = Activity.objects.filter(activity_type=Activity.LIKE, feed=self.pk)
        return likes

    def get_likers(self):
        """
        Get User objects which likes `feed`
        :return: QuerySet containing Users which likes `feed`
        :rtype: QuerySet
        """
        likes = self.get_likes()
        likers = []
        for like in likes:
            likers.append(like.user)
        return likers

    def calculate_comments(self):
        """
        :return: Number of comments for `feed`
        :rtype: int
        """
        self.comments = Feed.objects.filter(parent=self).count()
        self.save()
        return self.comments

    def comment(self, user, post):
        """
        Function for save comment and increment numbers of comments of a `feed`
        :param user: Author of a comment
        :type user: object
        :param post: Text of a comment
        :type post: str
        :return: Saved comment
        :rtype: object

        Args:
            user (TYPE): Description
            post (TYPE): Description
        """
        feed_comment = Feed(user=user, post=post, parent=self)
        feed_comment.save()
        self.comments = Feed.objects.filter(parent=self).count()
        self.save()
        return feed_comment

    def linkfy_post(self):
        """
        Convert URL-like strings in an HTML fragment to links.
        :return: Converts plain text into HTML with links
        :rtype: str
        """
        return bleach.linkify(escape(self.post))
