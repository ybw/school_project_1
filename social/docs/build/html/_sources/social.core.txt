core Package
============

:mod:`forms` Module
-------------------

.. automodule:: social.core.forms
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`views` Module
-------------------

.. automodule:: social.core.views
    :members:
    :undoc-members:
    :show-inheritance:

