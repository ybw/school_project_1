.. Robin's Nest documentation master file, created by
   sphinx-quickstart on Thu Feb 25 07:41:51 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Robin's Nest's documentation!
========================================

Contents:

.. toctree::
   :maxdepth: 2
   :includehidden:

   test_social

.. note::

   This function is not suitable for sending spam e-mails.
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

