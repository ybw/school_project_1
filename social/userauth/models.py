"""Summary
"""
import hashlib
import os.path
import urllib.error
import urllib.parse
import urllib.request

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save

from social.activities.models import Notification

from sorl.thumbnail import ImageField


class Profile(models.Model):
    """Summary
    """
    user = models.OneToOneField(User)
    location = models.CharField(max_length=50, null=True, blank=True)
    url = models.CharField(max_length=50, null=True, blank=True)
    job_title = models.CharField(max_length=50, null=True, blank=True)
    follows = models.ManyToManyField('self', related_name='followers',
                                     symmetrical=False)
    picture = ImageField(upload_to='avatars', blank=True, null=True)
    picture_update_dt = models.DateTimeField(blank=True, null=True)
    address = models.CharField(max_length=50, null=True, blank=True)
    telephone = models.CharField(max_length=50, null=True, blank=True)
    workplace = models.CharField(max_length=50, null=True, blank=True)

    def get_url(self):

        url = self.url
        if "http://" not in self.url and "https://" not in self.url and len(
                self.url) > 0:
            url = "http://" + str(self.url)
        return url

    def get_picture(self):
        """Summary
        """
        no_picture = 'http://trybootcamp.vitorfs.com/static/img/user.png'
        try:
            filename = settings.MEDIA_ROOT + '/profile_pictures/' + \
                       self.user.username + '.jpg'
            picture_url = settings.MEDIA_URL + 'profile_pictures/' + \
                          self.user.username + '.jpg'
            if os.path.isfile(filename):
                return picture_url
            else:
                gravatar_url = 'http://www.gravatar.com/avatar/{0}?{1}'.format(
                    hashlib.md5(self.user.email.lower()).hexdigest(),
                    urllib.parse.urlencode({'d': no_picture, 's': '256'})
                )
                return gravatar_url
        except Exception as e:
            return no_picture

    def get_screen_name(self):
        """Summary
        """
        try:
            if self.user.get_full_name():
                return self.user.get_full_name()
            else:
                return self.user.username
        except:
            return self.user.username

    def notify_liked(self, feed):
        """Summary
        """
        if self.user != feed.user:
            Notification(notification_type=Notification.LIKED,
                         from_user=self.user,
                         to_user=feed.user,
                         feed=feed).save()

    def unotify_liked(self, feed):
        """Summary
        """
        if self.user != feed.user:
            Notification.objects.filter(notification_type=Notification.LIKED,
                                        from_user=self.user,
                                        to_user=feed.user,
                                        feed=feed).delete()

    def notify_commented(self, feed):
        """Summary
        """
        if self.user != feed.user:
            Notification(notification_type=Notification.COMMENTED,
                         from_user=self.user,
                         to_user=feed.user,
                         feed=feed).save()

    def notify_also_commented(self, feed):
        """Summary
        """
        comments = feed.get_comments()
        users = []
        for comment in comments:
            if comment.user != self.user and comment.user != feed.user:
                users.append(comment.user.pk)
        users = list(set(users))
        for user in users:
            Notification(notification_type=Notification.ALSO_COMMENTED,
                         from_user=self.user,
                         to_user=User(id=user),
                         feed=feed).save()




def create_user_profile(sender, instance, created, **kwargs):
    """Summary
    """
    if created:
        Profile.objects.create(user=instance)


def save_user_profile(sender, instance, **kwargs):
    """Summary
    """
    instance.profile.save()


post_save.connect(create_user_profile, sender=User)
post_save.connect(save_user_profile, sender=User)
