"""Summary
"""
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static

urlpatterns = patterns('',
                       url(r'^$', 'social.core.views.home', name='home'),
                       url(r'^login', 'django.contrib.auth.views.login', {'template_name': 'core/cover.html'},
                           name='login'),
                       url(r'^logout', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),
                       url(r'^signup/$', 'social.userauth.views.signup', name='signup'),
                       url(r'^settings/$', 'social.core.views.settings', name='settings'),
                       url(r'^settings/picture/$', 'social.core.views.picture', name='picture'),
                       url(r'^settings/upload_picture/$', 'social.core.views.upload_picture', name='upload_picture'),
                       url(r'^settings/save_uploaded_picture/$', 'social.core.views.save_uploaded_picture',
                           name='save_uploaded_picture'),
                       url(r'^settings/password/$', 'social.core.views.password', name='password'),
                       url(r'^network/$', 'social.core.views.network', name='network'),
                       url(r'^friends/$', 'social.core.views.friends', name='friends'),
                       url(r'^recommended/$', 'social.core.views.recommended_friends', name='recommended'),
                       url(r'^feeds/', include('social.feeds.urls')),
                       url(r'^messages/', include('social.tweets.urls')),
                       url(r'^notifications/$', 'social.activities.views.notifications', name='notifications'),
                       url(r'^notifications/last/$', 'social.activities.views.last_notifications',
                           name='last_notifications'),
                       url(r'^notifications/check/$', 'social.activities.views.check_notifications',
                           name='check_notifications'),
                       url(r'^search/$', 'social.search.views.search', name='search'),
                       url(r'^(?P<username>[^/]+)/$', 'social.core.views.profile', name='profile'),

                       ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
