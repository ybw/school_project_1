pip install -r requirements.txt
python manage.py migrate --run-syncdb



C:\projects\social>pip freeze
alabaster==0.7.7
Babel==2.2.0
bleach==1.4
colorama==0.3.6
dj-database-url==0.3.0
dj-static==0.0.6
Django==1.9.2
django-photologue==3.4.1
django-sortedm2m==1.1.1
docutils==0.12
ExifRead==2.1.2
gunicorn==19.3.0
html5lib==0.9999999
Jinja2==2.8
Markdown==2.4.1
MarkupSafe==0.23
pillow==3.1.1
Pygments==2.1.1
python-decouple==2.2
pytz==2015.7
six==1.10.0
snowballstemmer==1.2.1
Sphinx==1.3.5
sphinx-rtd-theme==0.1.9
static3==0.6.1
Unipath==1.1
virtualenv==14.0.6

C:\projects\social>